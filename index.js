const fs = require('fs');
const compareObj = require('./compare');


if (process.argv.length != 4) {
  console.log('Usage: node compare.js jsonA jsonB');
  process.exit(1);
}

let a, b; 

try {
  a = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));
  b = JSON.parse(fs.readFileSync(process.argv[3], 'utf8'));
} catch (error) {
  console.log('There was an error while trying to read your files.');
  process.exit(1);
}

console.log(compareObj(a, b));
