var assert = require('assert');
const compareObj = require('../compare');

describe('compareObj', function () {
  describe('Basic tests', function () {
    it('should return 1 when comparing equal primitives', function () {
      assert.equal(compareObj(0, 0), 1);
      assert.equal(compareObj(0.5, 0.5), 1);
      assert.equal(compareObj('', ''), 1);
      assert.equal(compareObj(null, null), 1);
    });

    it('should return 0 when both values are of different type', function () {
      assert.equal(compareObj('', {}), 0);
      assert.equal(compareObj(0, ''), 0);
      assert.equal(compareObj([], ''), 0);
    });

    it('should not try to compare primitives to objects', function () {
      assert.equal(compareObj('', {}), 0);
      assert.equal(compareObj(0, {}), 0);
      assert.equal(compareObj(null, {}), 0);
      assert.equal(compareObj('', []), 0);
      assert.equal(compareObj(0, []), 0);
      assert.equal(compareObj(null, []), 0);
    });

    it('should not try to compare primitives to objects in reverse order', function () {
      assert.equal(compareObj({}, ''), 0);
      assert.equal(compareObj({}, 0), 0);
      assert.equal(compareObj({}, null), 0);
      assert.equal(compareObj([], ''), 0);
      assert.equal(compareObj([], 0), 0);
      assert.equal(compareObj([], null), 0);
    });

    it('should not consider null an object', function () {
      assert.equal(compareObj(null, {}), 0);
      assert.equal(compareObj({}, null), 0);
    });

    it('should not consider arrays same type as objects', function () {
      assert.equal(compareObj([], {}), 0);
      assert.equal(compareObj({}, []), 0);
    });
  });

  describe('Equality tests', () => {
    it('should consider arrays equal only if their contents are equal', function () {
      assert.equal(compareObj([], []), 1);
      assert.equal(compareObj([0], [0]), 1);
      assert.equal(compareObj([1, 2], [1, 2]), 1);
    });

    it('should consider arrays not equal if their contents are different', function () {
      assert.equal(compareObj([1, 2], [2, 1]), 0);
      assert.equal(compareObj([1], []), 0);
      assert.equal(compareObj([], [1]), 0);
    });

    it('should consider objects equal only if their contents are equal', function () {
      assert.equal(compareObj({}, {}), 1);
      assert.equal(compareObj({ a: 1 }, { a: 1 }), 1);
      assert.equal(compareObj({ a: 1 }, { a: '' }), 0);
      assert.equal(compareObj({ a: 1, b: 1 }, { b: 1, a: 1 }), 1);
      assert.equal(compareObj({ a: 1, b: 1 }, { b: [], a: 0 }), 0);
    });

    it('should consider objects not equal if their contents are different', function () {
      assert.equal(compareObj({}, { a: 1 }), 0);
      assert.equal(compareObj({ a: 1 }, { a: 0 }), 0);
      assert.equal(compareObj({ a: 2, b: 2 }, { b: 1, a: 1 }), 0);
    });

    it('should correctly compare primitives', function () {
      assert.equal(compareObj('', ''), 1);
      assert.equal(compareObj('', 'asdsad'), 0);
      assert.equal(compareObj('asdsad', 'asdsad'), 1);
      assert.equal(compareObj(434, 434), 1);
      assert.equal(compareObj(434, 4345), 0);
    });
  });

  describe('Score Tests', () => {
    it('should return 0 for different objects', function () {
      assert.equal(compareObj({}, { a: 1 }), 0);
      assert.equal(compareObj([], [0]), 0);
    });
    it('should return 1 for objects of same content', function () {
      assert.equal(compareObj({ a: 1 }, { a: 1 }), 1);
      assert.equal(compareObj([0], [0]), 1);
    });
    it('should return an accurate score based on the contents', function () {
      assert.equal(compareObj({ a: 1 }, { a: 1, b: 0 }), 1/2);
      assert.equal(compareObj({ a: 1, b: 1 }, { a: 1, b: 1, c: 1, d: 1 }), 1/2);
      assert.equal(compareObj({ a: 1 }, { a: 1, b: 0, c: '2' }), 1 / 3);
      assert.equal(compareObj([0], [0, 0, 0]), 1 / 3);
    });
  });
  describe('Nested Tests', () => {
    it('should return an accurate score for nested objects', function () {
      assert.equal(compareObj({ a: {a:0} }, { a: {a:0} }), 1);
      assert.equal(compareObj({ a: {a:0} }, { a: {a:1} }), 0);
      assert.equal(compareObj({ a: {a:0} }, { a: {a:0,b:1} }), 1/2);
      assert.equal(compareObj({ a: {a:0} }, { a: {a:0,b:1},b:1 }), 1/2*1/2 );
      assert.equal(compareObj([[0]], [[0,1]]), 1/2);
    });
    it('should return an accurate score for bigger objects', function () {
      assert.equal(compareObj({ a: {a:[0]} }, { a: {a:[0]} }), 1);
      assert.equal(compareObj({ a: {a:[0]} }, { a: {a:[0,1]} }), 1/2);
      assert.equal(compareObj({ a: {a:{a: [0]}} }, { a: {a: {a: [0,1]}} }), 1/2);
    });
  });

  describe('Sample Tests', () => {
    const s1 = {
      name: "John",
      last_name: 'Joe',
      id: 3
    }
    /* Different last_name */
    const s2 = {
      name: "John",
      last_name: 'Doe',
      id: 3
    }
    /* Same as s1, just different order */
    const s3 = {
      last_name: 'Joe',
      name: "John",
      id: 3
    }
    /* Different city */
    const s4 = {
      last_name: 'Joe',
      name: "John",
      address:{
        street: "Fake street 123",
        city: "Texas"
      }
    }
    const s5 = {
      last_name: 'Joe',
      name: "John",
      address:{
        street: "Fake street 123",
        city: "NC"
      }
    }
    /* Same as s4 but different last_name */
    const s6 = {
      last_name: 'Doe',
      name: "John",
      address:{
        street: "Fake street 123",
        city: "NC"
      }
    }
    /* Array tests */
    const s7 = {
      last_name: 'Doe',
      name: "John",
      cart: [1,2,3,4]
    }
    /* one array item is different */
    const s8 = {
      last_name: 'Doe',
      name: "John",
      cart: [1,2,3,0]
    }
    /* Array with objects */
    const s9 = {
      last_name: 'Doe',
      name: "John",
      cart: [{id:1},{id:2},{id:3},{id: 4}]
    }
    /* one array item is different */
    const s10 = {
      last_name: 'Doe',
      name: "John",
      cart: [{id:1},{id:2},{id:3},{id: 0}]
    }
    it('should return an accurate score for sample cases', function () {
      assert.equal(compareObj(s1,s2), 2/3);
      assert.equal(compareObj(s1,s3), 1);
      assert.equal(compareObj(s4,s5), (2 + 1/2)/3);
      assert.equal(compareObj(s4,s6), (1 + 1/2)/3);
      assert.equal(compareObj(s5,s6), (1 + 2/2)/3);
      assert.equal(compareObj(s7,s8), (2 + 3/4)/3);
      assert.equal(compareObj(s9,s10), (2 + 3/4)/3);
    });
  });
});
