function compareObj(a, b) {
  let score = 0;
  if (typeof a === typeof b) {
    // primitives or null, direct comparison
    if (a === null || b === null || typeof a !== 'object')
      return Number(a === b);

    // either both are arrays or both are objects, otherwise skip
    if (
      (Array.isArray(a) && !Array.isArray(b)) ||
      (!Array.isArray(a) && Array.isArray(b))
    )
      return 0;

    // same type
    const aKeys = Object.keys(a);
    const bKeys = Object.keys(b);
    const larger = aKeys.length > bKeys.length ? aKeys : bKeys;

    for (let i = 0; i < larger.length; i++)
      score += compareObj(a[larger[i]], b[larger[i]]);
    if (larger.length) score /= larger.length;
    else score += 1;
  }
  return score;
}

module.exports = compareObj;
