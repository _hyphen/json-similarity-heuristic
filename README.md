# JSON Object Similarity Score

Write a program using JavaScript (Node) that will compare two json objects and give a score between 0 and 1 as to how similar they are, a score of 1 meaning the objects are identical. There are sample JSON files in the data directory for testing.